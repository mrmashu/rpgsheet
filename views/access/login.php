<h2>Log In</h2>
<?if(!empty($error)){?>
	<p class="error"><?=$error?></p>
<?}?>
<form action="/access/login" method="post">
	<div class="field">
		<label for="email">Email Address</label>
		<input autofocus type="email" name="email" id="email"/>
	</div>
	<div class="field">
		<label for="password">Password</label>
		<input type="password" name="password" id="password"/>
	</div>
	<input type="submit" value="Log In"/>
</form>
