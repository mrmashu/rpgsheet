<header>
	<h2><?=($xp->exists())?'Edit':'Add'?> XP</h2>
</header>

<section class="form">
	<?if($xp->exists()){?>
		<input type="hidden" name="xp_id" value="<?=$xp->id?>"/>
	<?}else{?>
		<input type="hidden" name="character_id" value="<?=$xp->character_id?>"/>
	<?}?>
	<div class="field"><?=$xp->amount->view()?></div>
	<div class="field"><?=$xp->notes->view()?></div>
	<div class="field"><?=$xp->date->view()?></div>
</section>

<footer>
	<a class="button" onclick="modal.close();">Cancel</a>
	<a class="action button" onclick="xp.save();">Save</a>
</footer>