<header>
	<h2>XP Log</h2>
	<a class="add icon" onclick="xp.add('<?=$character->id?>');"></a>
</header>

<section>
	<table>
		<colgroup>
			<col style="width:6rem;"/>
			<col style="width:6rem;"/>
			<col/>
			<col style="width:4rem;"/>
		</colgroup>
		<thead>
			<tr>
				<th>Amount</th>
				<th>Date</th>
				<th>Notes</th>
				<td>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?foreach($xps as $xp){?>
				<tr>
					<td><?=$xp->amount->get()?></td>
					<td><?=$xp->date->format('m/d/Y')?></td>
					<td><?=$xp->notes->get()?></td>
					<td>
						<a onclick="xp.edit('<?=$xp->id?>');">edit</a>
						<a onclick="xp.delete('<?=$xp->id?>', '<?=$character->id?>');">del</a>
					</td>
				</tr>
			<?}?>
		</tbody>
	</table>
</section>
