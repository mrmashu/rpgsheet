<header>
	<h2>Edit Stat</h2>
</header>

<section class="form">
	<div class="field"><?=$stat->name->view()?></div>
	<table>
		<thead>
			<tr>
				<th>Value</th>
				<th>Notes</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?foreach($stat->vals() as $statval){?>
				<tr>
					<td><input type="text" name="statvals[<?=$statval->id?>][val]" value="<?=$statval->val->get()?>"/></td>
					<td><input type="text" name="statvals[<?=$statval->id?>][notes]" value="<?=$statval->notes->get()?>"/></td>
					<td>
						<a onclick="stats.deleteval('<?=$statval->id?>');">del</a>
					</td>
				</tr>
			<?}?>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td></td>
				<td>
					<a onclick="stats.addval('<?=$stat->id?>');">add</a>
				</td>
			</tr>
		</tfoot>
	</table>
</section>

<footer>
	<a class="button" onclick="modal.close();">Cancel</a>
	<a class="caution button" onclick="stats.delete('<?=$stat->id?>');">Delete</a>
	<a class="action button" onclick="stats.save();">Save</a>
</footer>