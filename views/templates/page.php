<!DOCTYPE html>
<html>
<head>
	<title>RPG Sheet</title>
	<link rel="stylesheet" href="/css/styles.css"/>
	<link rel="stylesheet" href="/css/admin/modal.css"/>
	<script src="/js/jquery.min.js"></script>
	<script src="/js/admin/admin.js"></script>
	<script src="/js/admin/modal.js"></script>
	<script src="/js/scripts.js"></script>
	<script src="/js/stats.js"></script>
	<script src="/js/dice.js"></script>
	<script src="/js/xp.js"></script>
	<?=f()->template->headtags()?>
	<?=f()->tag->canonical()?>
</head>
<body>
	<header>
		<h1>RPG Sheet</h1>
		<nav>
			<?if(f()->access->isloggedin()){?>
				<a href="/characters">Characters</a>
				<a href="/access/logout">Log Out</a>
			<?}else{?>
				<a href="/access/login">Log In</a>
			<?}?>
		</nav>
	</header>
	<main><?=$content?></main>
</body>
</html>