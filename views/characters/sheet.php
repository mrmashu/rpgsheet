<?php

// define which sections go in which cols
$cols = array(
	[
		'name',
		'stats',
	],
	[
		'values',
	],
	[
		'xp',
	],
);

// output each col
foreach($cols as $col){
	?><div class="col"><?
	foreach($col as $section){
		echo f()->view->load('characters/sections/'.$section, array(
			'character'=>$character,
		));
	}
	?></div><?
}