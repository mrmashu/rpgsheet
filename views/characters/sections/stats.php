<section class="stats">
	<header>
		<h2>Stats</h2>
		<a class="add icon" onclick="stats.add();"></a>
	</header>
	<table>
		<thead>
			<tr>
				<th>Stat</th>
				<th>Score</th>
				<th>Mod</th>
				<th>Actions</th>
			</tr>
		</thead>
		<?foreach($character->stats() as $stat){?>
			<tr>
				<th><?=$stat->name?></th>
				<td><?=$stat->score()?></td>
				<td><?=$stat->mod()?></td>
				<td>
					<a onclick="stats.edit('<?=$stat->id?>');">edit</a>
					<a onclick="dice.roll('<?=$stat->name?> save:1d20+<?=$stat->mod()?>');">roll save</a>
				</td>
			</tr>
		<?}?>
	</table>
</section>