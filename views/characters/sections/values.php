<section class="values">
	<header>
		<h2>Values</h2>
		<a class="add icon" onclick="values.add();"></a>
	</header>
	<table>
		<colgroup>
			<col/>
			<col/>
			<col style="width:3rem;"/>
		</colgroup>
		<?foreach($character->values() as $value){?>
			<tr>
				<th><?=$value->name?></th>
				<td><input type="number" value="<?=$value->amount?>" data-value_id="<?=$value->id?>" data-amount="<?=$value->amount?>"/></td>
				<td><a onclick="values.delete('<?=$value->id?>');return false;" href="#">del</a></td>
			</tr>
		<?}?>
	</table>
</section>
