<header>
	<h2>Characters</h2>
	<a href="/characters/create">Create New Character</a>
</header>
<ul>
	<?foreach($characters as $character){?>
		<li><a href="/characters/show/<?=$character->id?>"><?=$character->name?></a></li>
	<?}?>
</ul>