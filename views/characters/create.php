<h2>Create New Character</h2>
<?if(!$character->isvalid()){?>
	<p class="error"><?=$character->errormessage()?></p>
<?}?>
<form action="<?=f()->url->current()?>" method="post">
	<div class="field"><?=$character->name->view()?></div>
	<input type="submit" name="commit" value="Create Character"/>
</form>
