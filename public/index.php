<h2>RPG Sheet</h2>

<p>This is a little system for managing RPG character sheets.</p>

<?if(f()->access->isloggedin()){?>
	<p>You are already logged in. Check out your <a href="/characters">Characters</a></p>
<?}else{?>
	<p>To get started, <a href="/access/login">Log In</a></p>
<?}?>