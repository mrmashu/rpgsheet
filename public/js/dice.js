var dice = {};
dice.roll = function(formula){
	var results = [];
	var dicesets = formula.split(',');
	for(var i in dicesets){
		var diceset = dice.strip_label(dicesets[i]);
		var result = {};
		result.label = diceset[0];
		result.total = 0;
		var terms = diceset[1].split('+');
		for(var j in terms){
			var term = terms[j];
			result.total += dice.evaluate_term(term);
		}
		results.push(result);
	}
	dice.display(results, formula);
};




// evaluates 1 term and returns the result.
// these can be in the form "ndm"
//   where n is an int
//   d is the literal d character
//   m is the max dice value
// it can also be a single integer, which should just return that.
// it can also be a string, which might be a stat.
// if it is none of the above, it is invalid.
dice.evaluate_term = function(term){
	// see if it's a normal dice roll:
	var d = term.match(/([0-9]+)d([0-9]+)/);
	if(d != null) return dice.roll_dice(d[1], d[2]);

	// see if it's a number:
	var m = term.match(/^(-?[0-9]+)$/);
	if(m != null) return Number(m[1]);

	// TODO see if it's a stat or a value

	console.log('invalid term '+term);
	return 0;
};


// gets a label for a term, and returns the term with a potential label removed from it
// returns an array where the first element is the label for this term
// the second element is the term without a label
dice.strip_label = function(diceset){
	var colonPos = diceset.indexOf(':');
	if(colonPos < 0) return [diceset, diceset];
	var label = diceset.substr(0, colonPos).trim();
	var rest = diceset.substr(colonPos+1).trim();
	return [label, rest];
};


dice.display = function(results, formula){
	var html = '<header><h2>Dice Roller</h2></header>';

	html += '<section class="form">';
	html += '<input type="text" id="formula"';
	if(typeof(formula)!='undefined') html += ' value="'+formula+'"';
	html += '/>';
	html += '<a class="action button">roll</a>';
	html += '</section>';

	// display results
	if(typeof(results)!='undefined'){
		html += '<table>';
		html += '<thead><tr><th>Label</th><th>Result</th></tr></thead>';
		html += '<tbody>';
		for(var i in results){
			html += '<tr>';
			html += '<td>'+results[i].label+'</td>';
			html += '<td>'+results[i].total+'</td>';
			html += '</tr>';
		}
		html += '</tbody>';
		html += '</table>';
	}

	// show the modal
	modal.html(html);

	// set up events in modal:
	$('.modal .action.button').on('click', function(){
		var formula = $('#formula').val();
		dice.roll(formula);
	});
	$('.modal input[type=text]').on('keydown', function(e){
		// enter key:
		if(e.keyCode == 13){
			var formula = $('#formula').val();
			dice.roll(formula);
		}
	});
};


// rolls some dice.
// quantity is how many dice.
// max is the max face value of the die.
// for example, to roll 2d20, quantity is 2, and max is 20
// returns a single Number that is the sum of all rolls
dice.roll_dice = function(quantity, max){
	// sanitize input:
	quantity = Number(quantity);
	max = Number(max);

	// run all dice rolls:
	var total = 0;
	for(var i = 0; i < quantity; i++){
		var r = Math.floor(Math.random() * max) + 1;
		total += r;
	}
	return total;
};


$(function(){
	$(document).keypress(function(e){
		// 100 = d
		if(e.charCode == 100){
			if(document.activeElement == document.getElementsByTagName('body')[0]){
				dice.display();
			}
		}
	});
});