var sheet = {};


// get the current character_id if we're on the character sheet page
if(location.href.match(/characters\/show\/[0-9]+$/)){
	sheet.character_id = location.href.match(/characters\/show\/([0-9])$/)[1];
}else{
	sheet.character_id = 0;
}


sheet.refresh = function(){
	var data = {};
	data.character_id = sheet.character_id;
	$.post('/characters/sheet', data, function(response){
		$('#sheet').html(response);
	});
};

var values = {};
values.add = function(){
	var data = {};
	data.character_id = sheet.character_id;
	data.name = prompt('What is the name for this value?');
	if(data.name != null){
		$.post('/values/add', data, sheet.refresh);
	}
};
values.delete = function(value_id){
	var data = {};
	data.character_id = sheet.character_id;
	data.value_id = value_id;
	$.post('/values/delete', data, sheet.refresh);
};
values.save = function(ele){
	var $ele = $(ele);
	var oldval = $ele.attr('data-amount');
	var newval = $ele.val();
	if(oldval != newval){
		var data = {};
		data.value_id = $ele.attr('data-value_id');
		data.amount = newval;
		data.character_id = sheet.character_id;
		$.post('/values/save', data, function(response){
			if(response == ''){
				$ele.attr('data-amount', newval);
				sheet.refresh();
			}
		});
	}
};

$(function(){
	sheet.refresh();
	$('body').on('mouseover', 'section', function(){
		$(this).closest('section').addClass('hover');
	});
	$('body').on('mouseout', 'section', function(){
		$(this).closest('section').removeClass('hover');
	});
	$('body').on('blur', 'section.values input', function(){
		values.save(this);
	});
	$('body').on('keydown', 'section.values input', function(e){
		if(e.keyCode==13){ // 13 = enter key
			values.save(this);
		}
	});
});