var xp = {};
xp.add = function(character_id){
	var data = {};
	data.character_id = character_id;
	$.post('/xp/edit', data, function(html){
		modal.html(html);
	});
};
xp.edit = function(xp_id){
	var data = {};
	data.xp_id = xp_id;
	$.post('/xp/edit', data, function(html){
		modal.html(html);
	});
};
xp.showlog = function(character_id){
	modal.get('/xp/log/'+character_id);
};
xp.save = function(){
	var data = getFormData('.modal');
	data.character_id = xp.character_id;
	$.post('/xp/save', data, function(){
		modal.close();
		sheet.refresh();
	});
};
xp.delete = function(xp_id, character_id){
	if(confirm('Are you sure you want to delete this xp?')){
		$.post('/xp/delete', {xp_id:xp_id}, function(){
			xp.showlog(character_id);
			sheet.refresh();
		});
	}
};
