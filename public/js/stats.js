var stats = {};
stats.currentstat_id = 0;

stats.add = function(){
	var data = {};
	data.character_id = sheet.character_id;
	data.name = prompt('What is the name of the new stat?');
	if(data.name != null){
		$.post('/stats/add', data, sheet.refresh);
	}
};
stats.edit = function(stat_id){
	stats.currentstat_id = stat_id;
	modal.get('/stats/edit/'+stat_id);
};
stats.save = function(){
	var data = getFormData('.modal');
	$.post('/stats/edit/'+stats.currentstat_id, data, function(){
		modal.close();
		sheet.refresh();
	});
};
stats.delete = function(stat_id){
	if(confirm('Are you sure you want to delete this stat?')){
		var data = {};
		data.stat_id = stat_id;
		modal.close();
		$.post('/stats/delete', data, function(){
			sheet.refresh();
		});
	}
};

stats.addval = function(stat_id){
	var data = {};
	data.stat_id = stat_id;
	$.post('/stats/addval', data, function(){
		stats.edit(stat_id);
	});
};
stats.deleteval = function(statval_id){
	if(confirm('Are you sure you want to delete this value?')){
		var data = {};
		data.statval_id = statval_id;
		$.post('/stats/deleteval', data, function(){
			stats.edit(stat_id);
		});
	}
};
