<?php
namespace models;

use models\stat;

class statval extends \funky\model
{
	public function stat(){
		return stat::fromid($this->stat_id);
	}
	public static function fields(){
		return f()->load->fields([
			['val', 'integer'],
			['notes', 'text'],
			['stat_id', 'reference'],
		]);
	}
}