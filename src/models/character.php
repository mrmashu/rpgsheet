<?php
namespace models;

class character extends \funky\model
{
	public function values(){
		return \models\value::query()->where('character_id = '.$this->id)->orderby('sort_id', 'asc');
	}
	public function stats(){
		return \models\stat::query()->where('character_id = '.$this->id)->orderby('sort_id', 'asc');
	}
	public function recentxp(){
		return \models\xp::query()->where('character_id = '.$this->id)->orderby('date desc')->limit(3);
	}
	public function totalxp(){
		return f()->db->query('select sum(amount) as total from xps where character_id = '.$this->id)->val('total');
	}
	// returns true or false depending on if the current user has access to this character
	public function hasaccess(){
		if($this->user_id == f()->access->user_id()) return true;
		return false;
	}
	public static function fields(){
		return f()->load->fields([
			['name', 'text'],
			['user_id', 'reference'],
		]);
	}
}