<?php
namespace models;

class stat extends \funky\model
{
	public function vals(){
		return \models\statval::query()->where('stat_id = '.$this->id);
	}
	public function character(){
		return \models\character::fromid($this->character_id->get());
	}
	// calculates and returns the mod
	// based on the score
	public function mod(){
		return floor(($this->score() - 10) / 2);
	}
	public function score(){
		$score = 0;
		foreach($this->vals() as $statval){
			$score += $statval->val->get();
		}
		return $score;
	}
	public static function fields(){
		return f()->load->fields([
			['name', 'text'],
			['character_id', 'reference'],
			['sort_id', 'integer'],
		]);
	}
	public function delete(){
		// delete all stat vals, too.
		foreach($this->vals() as $statval){
			$statval->delete();
		}
		parent::delete();
	}
}