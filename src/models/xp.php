<?php
namespace models;

class xp extends \funky\model
{
	public function character(){
		return \models\character::fromid($this->character_id);
	}
	public static function fields(){
		return f()->load->fields([
			['character_id', 'reference'],
			['amount', 'integer'],
			['notes', 'text'],
			['date', 'date'],
		]);
	}
}