<?php
namespace models;

class value extends \funky\model
{
	public function character(){
		return \models\character::fromid($this->character_id);
	}
	public static function fields(){
		return f()->load->fields([
			['character_id', 'reference'],
			['sort_id', 'integer'],
			['name', 'text'],
			['amount', 'integer'],
		]);
	}
}