<?php
namespace controllers;

use models\character;
use models\value;

class values{
	public function __construct(){
		f()->access->enforce('player', '/access/login');
	}
	public function add(){
		if(empty($_POST['character_id'])) die('no character_id given');
		if(empty($_POST['name'])) die('no name given');
		$character = character::fromid($_POST['character_id']);
		if(!$character->hasaccess()) die('you do not have access to this character');
		$sort_id = f()->db->query('select max(sort_id) as sort_id from `values` where character_id = '.$_POST['character_id'])->val('sort_id');
		$value = new value();
		$value->update([
			'character_id'=>$_POST['character_id'],
			'sort_id'=>$sort_id+1,
			'name'=>$_POST['name'],
		]);
		return true;
	}
	public function delete(){
		if(empty($_POST['character_id'])) die('no character_id given');
		if(empty($_POST['value_id'])) die('no value_id given');
		$value = value::fromid($_POST['value_id']);
		if($value->character_id == $_POST['character_id']) $value->delete();
		f()->response->ok();
	}
	public function save(){
		if(empty($_POST['character_id'])) die('no character_id given');
		if(empty($_POST['value_id'])) die('no value_id given');
		if(!isset($_POST['amount'])) die('no amount given');
		$value = value::fromid($_POST['value_id']);
		if($value->character_id != $_POST['character_id']) die('wrong character_id given');
		$character = $value->character();
		if(!$character->hasaccess()) die('you dont have permission to save this value');
		$value->update([
			'amount'=>$_POST['amount'],
		]);
		f()->response->ok();
	}
}