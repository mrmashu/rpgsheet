<?php
namespace controllers;

use models\character;

class characters{
	public function __construct(){
		f()->access->enforce();
	}
	public function index(){
		$characters = character::query()->where('user_id = '.f()->access->user_id());
		return f()->view->load('characters/index', array(
			'characters'=>$characters,
		));
	}
	public function create(){
		$character = new character();
		if(!empty($_POST)){
			$_POST['user_id'] = f()->access->user_id();
			$character->update($_POST);
			if($character->isvalid()){
				f()->response->redirect('/characters/show/'.$character->id);
			}
		}
		return f()->view->load('characters/create', array(
			'character'=>$character,
		));
	}
	public function sheet(){
		if(empty($_POST['character_id'])) die('no character_id given');
		$character = character::fromid($_POST['character_id']);
		if($character->user_id->get() != f()->access->user_id()) throw new \exception('you do not have permission to view character '.$id);
		return f()->view->load('characters/sheet', array(
			'character'=>$character,
		));
	}
	public function show($id){
		$character = character::fromid($id);
		if($character->user_id->get() != f()->access->user_id()) throw new \exception('you do not have permission to view character '.$id);
		return f()->view->load('characters/show', array(
			'character'=>$character,
		));
	}
}