<?php
namespace controllers;

class access{
	public function index(){
		if(f()->access->isloggedin()) f()->response->redirect('/characters');
		f()->response->redirect('/access/login');
	}
	public function login(){
		$error = '';
		if(!empty($_POST)){
			f()->access->login($_POST['email'], $_POST['password']);
			if(f()->access->isloggedin()){
				f()->response->redirect('/characters');
			}
			$error = 'Failed to authenticate. Please try again.';
		}
		return f()->view->load('access/login', array(
			'error'=>$error,
		));
	}
	public function logout(){
		f()->access->logout();
		return f()->view->load('access/logout');
	}
}