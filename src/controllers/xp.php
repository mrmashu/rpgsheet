<?php
namespace controllers;

use models\character;

class xp
{
	public function edit(){
		// make or load an xp object
		if(empty($_POST['xp_id'])){
			if(empty($_POST['character_id'])) return 'you must give either xp_id or character_id';
			$xp = new \models\xp();
			$xp->character_id = $_POST['character_id'];
			$xp->date = date('Y-m-d');
		}
		else{
			$xp = \models\xp::fromid($_POST['xp_id']);
			if(!$xp->character()->hasaccess()) return 'you do not have access to edit this xp';
		}

		// load view:
		return f()->view->load('xp/edit', [
			'xp'=>$xp,
		]);
	}
	public function save(){
		// load or create an xp model object
		if(!empty($_POST['xp_id'])){
			$xp = \models\xp::fromid($_POST['xp_id']);
		}else if(!empty($_POST['character_id'])){
			$xp = \models\xp::fromdata($_POST);
		}else{
			return 'must give xp_id or character_id';
		}

		// access check:
		if(!$xp->character()->hasaccess()) return 'you do not have access to this xp';

		// update the xp with all new info
		$xp->update($_POST);

		return 'xp saved';
	}
	public function log($character_id){
		$character = character::fromid($character_id);
		if(!$character->hasaccess()) return 'you do not have access to this characters xp log';
		$xps = \models\xp::query()->where('character_id = '.$character->id)->orderby('date desc');
		return f()->view->load('xp/log', [
			'character'=>$character,
			'xps'=>$xps,
		]);
	}
	public function delete(){
		$xp = \models\xp::fromid($_POST['xp_id']);
		if(!$xp->character()->hasaccess()) return 'you do not have access to delete this xp';
		$xp->delete();
		return 'xp deleted';
	}
}
