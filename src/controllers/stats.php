<?php
namespace controllers;

use models\character;
use models\stat;
use models\statval;

class stats
{
	public function add(){
		$character = character::fromid($_POST['character_id']);
		if(!$character->hasaccess()) f()->response->redirect('/characters');
		$stat = stat::insert($_POST);
		return 'Stat added';
	}
	public function edit($stat_id){
		$stat = stat::fromid($stat_id);
		$character = $stat->character();
		if(!$character->hasaccess()) f()->response->redirect('/characters');

		// handle saving
		if(!empty($_POST)){
			// update name:
			$stat->update($_POST);

			// update stat vals:
			foreach($_POST['statvals'] as $statval_id=>$data){
				$statval = statval::fromid($statval_id);

				// don't update this stat val if we don't have access to the character:
				if(!$statval->stat()->character()->hasaccess()) continue;
				$statval->update($data);
			}
			return 'Stat updated';
		}

		return f()->view->load('stats/edit', [
			'stat'=>$stat,
		]);
	}
	public function delete(){
		$stat = stat::fromid($_POST['stat_id']);
		$character = $stat->character();
		if(!$character->hasaccess()) f()->response->redirect('/characters');

		$stat->delete();
		return 'stat deleted';
	}
	public function addval(){
		$stat = stat::fromid($_POST['stat_id']);
		if(!$stat->character()->hasaccess()) return 'You dont have access to add a value to that stat';

		// make the new statval
		statval::insert([
			'stat_id'=>$_POST['stat_id'],
		]);
		return 'stat val added';
	}
	public function deleteval(){
		$statval = statval::fromid($_POST['statval_id']);
		if(!$statval->stat()->character()->hasaccess()) return 'You dont have permission to delete this stat val';
		$statval->delete();
		return 'stat val deleted';
	}
}